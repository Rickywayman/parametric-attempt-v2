/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================

/** */
MainComponent::MainComponent (Audio& audio_)
 :  audio (audio_),
    filePlayerGui (audio.getFilePlayer()),
    lowpassSlider (audio.getLowpassFilter(), audio),
    highpassSlider(audio.getHighpassFilter(), audio),
    bandpassSlider(audio.getBandpassFilter(), audio)
{
    setSize (500, 700);
    addAndMakeVisible(filePlayerGui);
    
    //addAndMakeVisible(&lowpassSlider);
    //addAndMakeVisible(&highpassSlider);
    //addAndMakeVisible(&butterworthSlider);
    addAndMakeVisible(&bandpassSlider);
    
    addAndMakeVisible(filterType);
    filterType.addItem("Lowpass", 1);
    filterType.addItem("Highpass", 2);
    filterType.addItem("Butterworth Lowpass", 3);
    filterType.addItem("Bandpass Filter", 4);
    filterType.setSelectedId(1);
    startTimer(100);
}

/** */
MainComponent::~MainComponent()
{
    
}

/** */
void MainComponent::resized()
{
    Rectangle<int> r (getLocalBounds());
    filePlayerGui.setBounds (r.removeFromTop(20));
    
    lowpassSlider.setBounds(r.removeFromTop (r.getHeight() / 2));
    highpassSlider.setBounds(r);
    //butterworthSlider.setBounds(0, 400, Size, 500);
    bandpassSlider.setBounds(r);
    
    filterType.setBounds(10, 600, 400, 100);
}

/** */
// needed timer to call this function! because in my last program my Audio while loop was calling this fucntion.. it has no specific callback function!
void MainComponent::filterSelect()
{

if (filterType.getSelectedId() == 1)
{
    audio.getComboBox(1);
}
else if(filterType.getSelectedId() == 2)
{
    audio.getComboBox(2);
}
else if(filterType.getSelectedId() == 3)
{
    audio.getComboBox(3);
}
else if(filterType.getSelectedId() == 4)
{
    audio.getComboBox(4);
}
}

/** */
void MainComponent::timerCallback()
{
    //lowpassGain = lowpassSlider.getLowpassGain();
    //audio.setLowpassGain(lowpassGain);
    filterSelect();
    
    //highpassGain = highpassSlider.getHighpassGain();
    //audio.setHighpassGain(highpassGain);
}

//MenuBarCallbacks==============================================================
/** */
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}
/** */
PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}
/** */
void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

