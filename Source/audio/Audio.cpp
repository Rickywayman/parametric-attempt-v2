/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#define _USE_MATH_DEFINES

#include "Audio.h"

#include <math.h>

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    //load the filePlayer into the audio source
    audioSourcePlayer.setSource (&filePlayer);
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
//    float theta;
//    float beta;
//    float gamma;
//    float a0, a1, a2, b1, b2;
//    float Q = 0.707; //butterworth principle
//    
//    theta = ( 2 * M_PI * 1000 ) / 44100;
//    beta = (( 1 - tanf(theta / 2 * Q) ) / ( 1 + tanf(theta / 2 * Q))) * 0.5;
//    gamma = ( 0.5 + beta ) * cos(theta);
//    a0 = 0.5 - beta;
//    a1 = 0.0;
//    a2 = -(0.5 - beta);
//    b1 = -2 * gamma;
//    b2 = 2 * beta;
//    
//    DBG("theta = " << theta);
//    DBG("beta = " << beta);
//    DBG("gamma = " << gamma);
//    DBG("a0 = " << a0);
//    DBG("a1 = " << a1);
//    DBG("a2 = " << a2);
//    DBG("b1 = " << b1);
//    DBG("b2 = " << b2);
//    DBG("Q = " << Q);
    
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
}

void Audio::setLowpassGain(float val)
{
    //DBG ("Audio Lowpass Gain   " << val);
    lowpassGain = val;
}

void Audio::setHighpassGain(float val)
{
    //DBG ("Audio Highpass Gain   " << val);
    highpassGain = val;
}

void Audio::setBandpassQ(float val)
{
    bandpassQ = val;
}

void Audio::setBandpassGain(float val)
{
    bandpassGain = val;
}

void Audio::getComboBox(int val)
{
    //DBG("AudioComboBoxValue: " << val);
    comboBox = val;
    
}



/////////// When to use int Audio::getComboBox() orrrrr void Audio::getComboBox(int val)////////////

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    // get the audio from our file player - player puts samples in the output buffer
    audioSourcePlayer.audioDeviceIOCallback (inputChannelData, numInputChannels, outputChannelData, numOutputChannels, numSamples);
    
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    //float inSampL;
    //float inSampR;
    
    while(numSamples--)
    {
        float output = *inL;
        float input = *inL;
        //float input = filePlayer.getNextAudioBlock;

        
        //float output = inSampL;
        
            if (comboBox == 1) // Lowpass
            {
                output = lowpassFilter.filter1 (output) * lowpassGain;
                
            }
            else if(comboBox == 2) // Highpass
            {
                output = ((highpassFilter.filter1 (output)) + *inL) * highpassGain;
            }
            else if(comboBox == 3) // Butterworth
            {
                //inSampL = (butterworthFilter.filter(*outL) + butterworthFilter.forwardfilter(input)) * BWgain;
            }
            else if(comboBox == 4) // Bandpass
            {
                //output = ((highpassFilter.filter(output) + *inL) - lowpassFilter.filter(output)) * lowpassGain;
                //output = (lowpassFilter.filter1(output) - (highpassFilter.filter1(output) + *inL)) * lowpassGain;
                
                output = bandpassFilter.filter1(output)
                + bandpassFilter.filter2(output)
                + bandpassFilter.feedforwardFilter1(input)
                + bandpassFilter.feedforwardFilter2(input)
                + (input * bandpassGain);
            }
      
        //inSampR = *outL;
        
        *outL = output;//inSampL * 1.f;
        *outR = output;//inSampL * 1.f;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    audioSourcePlayer.audioDeviceAboutToStart (device);
}

void Audio::audioDeviceStopped()
{
    audioSourcePlayer.audioDeviceStopped();
}