/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"
#include "LowPassFilter.h"
#include "HighpassFilter.h"
#include "ButterworthFilter.h"
#include "BandpassFilter.h"

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    FilePlayer& getFilePlayer() { return filePlayer; }
    LowpassFilter& getLowpassFilter() { return lowpassFilter; }
    HighpassFilter& getHighpassFilter() { return highpassFilter; }
    BandpassFilter& getBandpassFilter() { return bandpassFilter; }
    
    
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    void audioDeviceStopped() override;
    
    void setLowpassGain(float val);
    
    void setHighpassGain(float val);
    
    void setBandpassQ(float val);
    
    void setBandpassGain(float val);

    void getComboBox(int val);
    
private:
    AudioDeviceManager audioDeviceManager;
    
    AudioSourcePlayer audioSourcePlayer;
    FilePlayer filePlayer;
    
    LowpassFilter lowpassFilter;
    HighpassFilter highpassFilter;
    //ButterworthFilter butterworthFilter;
    //BandpassFilter bandpassFilter;
    BandpassFilter bandpassFilter;
    
    float lowpassGain;
    float highpassGain;
    float bandpassGain;
    float bandpassQ;
    int comboBox;
    
};



#endif  // AUDIO_H_INCLUDED
