//
//  BandpassSlider.h
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 25/01/2016.
//
//

#ifndef __JuceBasicAudio__BandpassSlider__
#define __JuceBasicAudio__BandpassSlider__


#include "../JuceLibraryCode/JuceHeader.h"
//#include "LowPassFilter.h"
//#include "HighpassFilter.h"
#include "BandpassFilter.h"
#include "Audio.h"

class BandpassSlider :      public Component,
                            private Slider::Listener
{
public:
    
    BandpassSlider (BandpassFilter& bandpassFilter_,
                    Audio& audio_);
    ~BandpassSlider();
    
    void resized() override;
    
    float getLowpassFeedbackGain();
    
    float getLowpassGain();
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (BandpassSlider)
    
    void sliderValueChanged	(Slider * 	slider);
    
    //LowpassFilter& lowpassFilter;
    //HighpassFilter& highpassFilter;
    BandpassFilter& bandpassFilter;
    Audio& audioRef;
    
    Slider gainSlider;
    Slider feedbackGainSlider;
    Slider QSlider;
    Label  gainLabel;
    Label feedbackGainLabel;
    Label bandpassQLabel;
    
    float gain;
    float level;
    float cornerFrequency;
    float bandpassQ;
    
};





#endif /* defined(__JuceBasicAudio__BandpassSlider__) */
