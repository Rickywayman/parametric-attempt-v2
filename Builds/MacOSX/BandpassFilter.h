//
//  BandpassFilter.h
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 25/01/2016.
//
//

#ifndef __JuceBasicAudio__BandpassFilter__
#define __JuceBasicAudio__BandpassFilter__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

#include "DelayLineFilter.h"
#include "SecondDelayLineFilter.h"

#include "ForwardDelayLineFilter.h"
#include "SecondForwardDelayLineFilter.h"

/**
 Bandpass Filters ARE FUN BUT ANNOYING
 */
class BandpassFilter :  public DelayLineFilter,
                        public SecondDelayLineFilter,
                        public ForwardDelayLineFilter,
                        public SecondForwardDelayLineFilter

{
public:
    //==============================================================================
    /**
     Constructor
     */
    BandpassFilter();
    
    /**
     Destructor
     */
    ~BandpassFilter();
    //==============================================================================
    /**
     Bandpass Filters are fun
     */
    float filter2 (float input) override;
    float filter1 (float input) override;
    float feedforwardFilter1(float feedforwardInput1) override;
    float feedforwardFilter2(float feedforwardInput2);
    
    //void setFeedbackGain(float val);
    
    //friend void lowpassGain(LowpassFilter &cLowpassFilter);
    
private:
    //float feedbackGain;
    
    
};


#endif /* defined(__JuceBasicAudio__BandpassFilter__) */
