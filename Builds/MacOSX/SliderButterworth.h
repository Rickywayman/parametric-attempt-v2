//
//  SliderButterworth.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 14/01/2016.
//
//

#ifndef __JuceBasicWindow__SliderButterworth__
#define __JuceBasicWindow__SliderButterworth__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class SliderButterworth :    public Component
//public SliderListener
{
public:
    
    SliderButterworth();
    ~SliderButterworth();
    
    void resized() override;
    
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SliderButterworth)
    
    Slider BWgainSlider;
    Slider BWfeedbackGainSlider;
    Label  BWgainLabel;
    Label BWfeedbackGainLabel;
    
    
};


#endif /* defined(__JuceBasicWindow__SliderButterworth__) */
