//
//  SliderLP.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 14/01/2016.
//
//

#include "SliderLP.h"

/** Constructor */
SliderLP::SliderLP (LowpassFilter& lowpassFilter_, Audio& audio_) : lowpassFilter (lowpassFilter_), audioRef(audio_)
{
    // Gain Slider
    addAndMakeVisible(&gainSlider);
    gainSlider.setRange(0.0, 1.0, 0.01);
    gainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    gainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    gainSlider.setValue(1.0);
    //gainSlider.addListener(this);
    
    // Feedback Slider
    addAndMakeVisible(&feedbackGainSlider);
    feedbackGainSlider.setRange(0.0, 1.0, 0.01);
    feedbackGainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    feedbackGainSlider.addListener(this);
    feedbackGainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    feedbackGainSlider.setValue(1.0);
    
    // Gain Label
    gainLabel.setText("gain", dontSendNotification);
    gainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&gainLabel);
    gainSlider.addListener (this);
    
    // Feedback Label
    feedbackGainLabel.setText("feedback", dontSendNotification);
    feedbackGainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&feedbackGainLabel);
    
}
/** Destructor */
SliderLP::~SliderLP()
{
    
}

/** */
void SliderLP::resized()
{
    int sliderSize = (getWidth()-20)/3;
    if(sliderSize > (getHeight()-60)) sliderSize = getHeight() - 60;
    
    gainLabel.setBounds(10, 0, sliderSize, 20);
    feedbackGainLabel.setBounds(10+sliderSize+sliderSize, 0, sliderSize, 20);
    
    gainSlider.setBounds(10, 20, sliderSize, sliderSize);
    feedbackGainSlider.setBounds(10+sliderSize+sliderSize, 20, sliderSize, sliderSize);
}

/** */
void SliderLP::sliderValueChanged (Slider* slider)
{
    if (slider == &gainSlider)
    {
        gain = slider->getValue();
        gain = gain*gain*gain; //cubic rule
        audioRef.setLowpassGain(gain);
    }
    else if (slider == &feedbackGainSlider)
    {
        float feedbackGain = slider->getValue();
        feedbackGain = feedbackGain*feedbackGain*feedbackGain;
        lowpassFilter.setFeedbackGain1(feedbackGain);
        //DBG ("slider output   " << feedbackGain);
    }
}

/** Function to pass lowpassGain variable to MainComponent*/
float SliderLP::getLowpassGain()
{
    return gain;
}

// Class End
