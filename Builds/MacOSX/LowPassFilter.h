//
//  LowPassFilter.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 12/01/2016.
//
//

#ifndef __JuceBasicWindow__LowPassFilter__
#define __JuceBasicWindow__LowPassFilter__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "DelayLineFilter.h"

/**
 Class that runs a simple one-pole lowpass filter
 */
class LowpassFilter : public DelayLineFilter
                    
{
public:
    //==============================================================================
    /**
     Constructor
     */
    LowpassFilter();
    
    /**
     Destructor
     */
    ~LowpassFilter();
    //==============================================================================
    /**
     Function that executes a simple one-pole lowpass filter
     */
    float filter1 (float input) override;
    
    //void setFeedbackGain(float val);
    
    //friend void lowpassGain(LowpassFilter &cLowpassFilter);
    
private:
    //float feedbackGain;
    
    
};


#endif /* defined(__JuceBasicWindow__LowPassFilter__) */
