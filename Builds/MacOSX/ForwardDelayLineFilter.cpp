//
//  ForwardDelayLineFilter.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 13/01/2016.
//
//

#include "ForwardDelayLineFilter.h"


ForwardDelayLineFilter::ForwardDelayLineFilter()
{
    for (int counter = 0; counter < ASSUMEDSAMPLERATE; counter++)
        forwarddelayLine1[counter] = 0.f;
    forwardreadDelaySamples1 = 1.f;
    forwardwriteIndex1 = 0;
    forwardfeedbackGain1 = 0.f;
}
ForwardDelayLineFilter::~ForwardDelayLineFilter()
{
    
}

void ForwardDelayLineFilter::setForwardDelayInSamples1(float delayInSamples)
{
    if(delayInSamples >= 0.f && delayInSamples <= ASSUMEDSAMPLERATE-1)
        forwardreadDelaySamples1 = delayInSamples;
}

void ForwardDelayLineFilter::setforwardFeedbackGain1(float val)
{
    DBG ("First Forward Delay Input  " << val);
    if (val >= 0.f && val <= 1.0)
    {
        forwardfeedbackGain1 = val;
    }
}

float ForwardDelayLineFilter::forwarddelayLineRead1()
{
    float readIndex = forwardwriteIndex1 - forwardreadDelaySamples1;
    if (readIndex < 0)
        readIndex += ASSUMEDSAMPLERATE;
    
    int pos1 = static_cast<int>(readIndex);
    int pos2 = pos1 + 1;
    if(pos2 == ASSUMEDSAMPLERATE)
        pos2 = 0;
    
    float fraction = readIndex - static_cast<int>(pos1);
    float amprange = forwarddelayLine1[pos2] - forwarddelayLine1[pos1];
    float output = forwarddelayLine1[pos1] + (fraction * amprange);
    
    return output;
}

void ForwardDelayLineFilter::forwarddelayLineWrite1(float input)
{
    if(++forwardwriteIndex1 == ASSUMEDSAMPLERATE)
        forwardwriteIndex1 = 0;
    forwarddelayLine1[forwardwriteIndex1] = input;
}
