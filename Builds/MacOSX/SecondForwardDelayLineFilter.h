//
//  SecondForwardDelayLineFilter.h
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 25/01/2016.
//
//

#ifndef __JuceBasicAudio__SecondForwardDelayLineFilter__
#define __JuceBasicAudio__SecondForwardDelayLineFilter__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"


#define ASSUMEDSAMPLERATE 44100

class SecondForwardDelayLineFilter
{
public:
    //==============================================================================
    /**
     Constructor
     */
    SecondForwardDelayLineFilter();
    
    /**
     Destructor
     */
    virtual ~SecondForwardDelayLineFilter();
    //==============================================================================
    /**
     Set the delay time in samples of the delayline - can be fractional
     */
    void setForwardDelayInSamples2(float delayInSamples);
    
    /**
     Sets the feedback gain
     */
    void setforwardFeedbackGain2(float val);
    
    /**
     Function to perform the filter processing should call delayLineRead and delayLineWrite at some point
     in the process
     */
    virtual float feedforwardFilter2(float feedforwardInput2)=0;
    
protected:
    /**
     Reads from the delayline using linear interpolation - should be called once per filter call and prior
     to delayLineWrite or the delay will be one sample inacurate
     */
    float forwarddelayLineRead2();
    
    /**
     Writes to the delayline - should be called once per filter call and after to delayLineRead or
     the delay will be one sample inacurate
     */
    void forwarddelayLineWrite2(float input);
    float forwardfeedbackGain2;
    
private:
    float forwarddelayLine2[ASSUMEDSAMPLERATE];
    float forwardreadDelaySamples2;
    int forwardwriteIndex2;
    
    
    
};

#endif /* defined(__JuceBasicAudio__SecondForwardDelayLineFilter__) */
