//
//  SecondDelayLineFilter.h
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 25/01/2016.
//
//

#ifndef __JuceBasicAudio__SecondDelayLineFilter__
#define __JuceBasicAudio__SecondDelayLineFilter__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"


#define ASSUMEDSAMPLERATE 44100

class SecondDelayLineFilter
{
public:
    //==============================================================================
    /**
     Constructor
     */
    SecondDelayLineFilter();
    
    /**
     Destructor
     */
    virtual ~SecondDelayLineFilter();
    //==============================================================================
    /**
     Set the delay time in samples of the delayline - can be fractional
     */
    void setDelayInSamples2 (float delayInSamples);
    
    /**
     Sets the feedback gain
     */
    void setFeedbackGain2 (float val);
    
    /**
     Function to perform the filter processing should call delayLineRead and delayLineWrite at some point
     in the process
     */
    virtual float filter2 (float input)=0;
    
    float readDelaySamples2;
    
protected:
    /**
     Reads from the delayline using linear interpolation - should be called once per filter call and prior
     to delayLineWrite or the delay will be one sample inacurate
     */
    float delayLineRead2();
    
    /**
     Writes to the delayline - should be called once per filter call and after to delayLineRead or
     the delay will be one sample inacurate
     */
    void delayLineWrite2(float input);
    float feedbackGain2;
    
private:
    float delayLine2[ASSUMEDSAMPLERATE];
    
    int writeIndex2;
    
    
    
};


#endif /* defined(__JuceBasicAudio__SecondDelayLineFilter__) */
