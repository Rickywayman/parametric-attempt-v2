//
//  DelayLineFilter.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 12/01/2016.
//
//

#include "DelayLineFilter.h"

DelayLineFilter::DelayLineFilter()
{
    for (int counter = 0; counter < ASSUMEDSAMPLERATE; counter++)
        delayLine1[counter] = 0.f;
    readDelaySamples1 = 1.f;
    writeIndex1 = 0;
    feedbackGain1 = 0.f;
}
DelayLineFilter::~DelayLineFilter()
{
    
}

void DelayLineFilter::setDelayInSamples1(float delayInSamples)
{
    if(delayInSamples >= 0.f && delayInSamples <= ASSUMEDSAMPLERATE-1)
        readDelaySamples1 = delayInSamples;
}

void DelayLineFilter::setFeedbackGain1(float val)
{
    
     DBG ("First Delay Input  " << val);
    if (val >= 0.f && val <= 1.0)
    {
        feedbackGain1 = val;
    }
}

float DelayLineFilter::delayLineRead1()
{
    float readIndex = writeIndex1 - readDelaySamples1;
    if (readIndex < 0)
        readIndex += ASSUMEDSAMPLERATE;
    
    int pos1 = static_cast<int>(readIndex);
    int pos2 = pos1 + 1;
    if(pos2 == ASSUMEDSAMPLERATE)
        pos2 = 0;
    
    float fraction = readIndex - static_cast<int>(pos1);
    float amprange = delayLine1[pos2] - delayLine1[pos1];
    float output = delayLine1[pos1] + (fraction * amprange);
    
    return output;
}

void DelayLineFilter::delayLineWrite1(float input)
{
    if(++writeIndex1 == ASSUMEDSAMPLERATE)
        writeIndex1 = 0;
    delayLine1[writeIndex1] = input;
}
