//
//  SecondForwardDelayLineFilter.cpp
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 25/01/2016.
//
//

#include "SecondForwardDelayLineFilter.h"

SecondForwardDelayLineFilter::SecondForwardDelayLineFilter()
{
    for (int counter = 0; counter < ASSUMEDSAMPLERATE; counter++)
        forwarddelayLine2[counter] = 0.f;
    forwardreadDelaySamples2 = 1.f;
    forwardwriteIndex2 = 0;
    forwardfeedbackGain2 = 0.f;
}
SecondForwardDelayLineFilter::~SecondForwardDelayLineFilter()
{
    
}

void SecondForwardDelayLineFilter::setForwardDelayInSamples2(float delayInSamples)
{
    if(delayInSamples >= 0.f && delayInSamples <= ASSUMEDSAMPLERATE-1)
        forwardreadDelaySamples2 = delayInSamples;
}

void SecondForwardDelayLineFilter::setforwardFeedbackGain2(float val)
{
    DBG ("Second Forward Delay Input  " << val);
    if (val >= 0.f && val <= 1.0)
    {
        forwardfeedbackGain2 = val;
    }
}

float SecondForwardDelayLineFilter::forwarddelayLineRead2()
{
    float readIndex = forwardwriteIndex2 - forwardreadDelaySamples2;
    if (readIndex < 0)
        readIndex += ASSUMEDSAMPLERATE;
    
    int pos1 = static_cast<int>(readIndex);
    int pos2 = pos1 + 1;
    if(pos2 == ASSUMEDSAMPLERATE)
        pos2 = 0;
    
    float fraction = readIndex - static_cast<int>(pos1);
    float amprange = forwarddelayLine2[pos2] - forwarddelayLine2[pos1];
    float output = forwarddelayLine2[pos1] + (fraction * amprange);
    
    return output;
}

void SecondForwardDelayLineFilter::forwarddelayLineWrite2(float input)
{
    if(++forwardwriteIndex2 == ASSUMEDSAMPLERATE)
        forwardwriteIndex2 = 0;
    forwarddelayLine2[forwardwriteIndex2] = input;
}
