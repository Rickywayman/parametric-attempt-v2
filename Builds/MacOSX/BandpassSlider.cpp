//
//  BandpassSlider.cpp
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 25/01/2016.
//
//

#include "BandpassSlider.h"
#include <math.h>


/** Constructor */
BandpassSlider::BandpassSlider (BandpassFilter& bandpassFilter_,
                                Audio& audio_)
:

    bandpassFilter(bandpassFilter_),
    audioRef(audio_)
{
    // Gain Slider
    addAndMakeVisible(&gainSlider);
    gainSlider.setRange(0.0, 1.0, 0.01);
    gainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    gainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    gainSlider.setValue(1.0);
    //gainSlider.addListener(this);
    
    // Q Slider
    addAndMakeVisible(&QSlider);
    QSlider.setRange(0.0, 10.0, 0.01);
    QSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    QSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    QSlider.setValue(1.0);
    QSlider.addListener(this);
    
    // Feedback Slider
    addAndMakeVisible(&feedbackGainSlider);
    feedbackGainSlider.setRange(0.0, 20000.0, 0.01);
    feedbackGainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    feedbackGainSlider.addListener(this);
    feedbackGainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    feedbackGainSlider.setValue(1.0);
    
    // Gain Label
    gainLabel.setText("gain", dontSendNotification);
    gainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&gainLabel);
    gainSlider.addListener (this);
    
    // Feedback Label
    feedbackGainLabel.setText("feedback", dontSendNotification);
    feedbackGainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&feedbackGainLabel);
    
    // Bandpass Q Label
    bandpassQLabel.setText("Q", dontSendNotification);
    bandpassQLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&bandpassQLabel);
    
    
   
 

    
}
/** Destructor */
BandpassSlider::~BandpassSlider()
{
    
}

/** */
void BandpassSlider::resized()
{
    int sliderSize = (getWidth()-20)/3;
    if(sliderSize > (getHeight()-60)) sliderSize = getHeight() - 60;
    
    gainLabel.setBounds(10, 0, sliderSize, 20);
    feedbackGainLabel.setBounds(10+sliderSize+sliderSize, 0, sliderSize, 20);
    bandpassQLabel.setBounds(10+sliderSize, 0, sliderSize, 20);
    
    gainSlider.setBounds(10, 20, sliderSize, sliderSize);
    feedbackGainSlider.setBounds(10+sliderSize+sliderSize, 20, sliderSize, sliderSize);
    QSlider.setBounds(10+sliderSize, 20, sliderSize, sliderSize);
}

/** */
void BandpassSlider::sliderValueChanged (Slider* slider)
{
    if (slider == &gainSlider)
    {
        //gain = slider->getValue();
        //gain = gain*gain*gain; //cubic rule
        //audioRef.setLowpassGain(gain);
        //audioRef.setHighpassGain(gain);
        //audioRef.setBandpassGain(gain);
        //DBG("bandpassGain = " << gain);
    }
    else if (slider == &feedbackGainSlider)
    {
        //float feedbackGain = slider->getValue();
        //feedbackGain = feedbackGain*feedbackGain*feedbackGain;
        //float highFeedGain = feedbackGain -
        //lowpassFilter.setFeedbackGain1(feedbackGain);
        //highpassFilter.setFeedbackGain1(feedbackGain);
        //DBG ("slider output   " << feedbackGain);
        
        //bandpassFilter.setFeedbackGain1(feedbackGain);
        
        cornerFrequency = slider->getValue();
        //DBG("bandpass Frequency = " << cornerFrequency);
        
        float theta;
        float beta;
        float gamma;
        float a0, a1, a2, b1, b2;
        float Q = 0.707; //butterworth principle
        
        theta = ( 2 * M_PI * 1000 ) / 44100;
        beta = (( 1 - tanf(theta / 2 * Q) ) / ( 1 + tanf(theta / 2 * Q))) * 0.5;
        gamma = ( 0.5 + beta ) * cos(theta);
        a0 = 0.5 - beta;
        a1 = 0.0;
        a2 = -(0.5 - beta);
        b1 = -2 * gamma;
        b2 = 2 * beta;
        
        DBG("theta = " << theta);
        DBG("beta = " << beta);
        DBG("gamma = " << gamma);
        DBG("a0 = " << a0);
        DBG("a1 = " << a1);
        DBG("a2 = " << a2);
        DBG("b1 = " << b1);
        DBG("b2 = " << b2);
        DBG("Q = " << Q);
        
        audioRef.setBandpassGain(a0);
        bandpassFilter.setFeedbackGain1(b1);
        bandpassFilter.setFeedbackGain2(b2);
        bandpassFilter.setforwardFeedbackGain1(a1);
        bandpassFilter.setforwardFeedbackGain2(a2);
        
        
    }
    else if (slider == &QSlider)
    {
        //bandpassQ = slider->getValue();
        //audioRef.setBandpassQ(bandpassQ);
        //DBG("bandpass Q = " << bandpassQ);
        
    }
}

// Class End
