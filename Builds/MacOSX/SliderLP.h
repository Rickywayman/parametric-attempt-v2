//
//  SliderLP.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 14/01/2016.
//
//

#ifndef __JuceBasicWindow__SliderLP__
#define __JuceBasicWindow__SliderLP__

//#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "LowPassFilter.h"
#include "Audio.h"

class SliderLP :    public Component,
                    private Slider::Listener
{
public:
    
    SliderLP (LowpassFilter& lowpassFilter_, Audio& audio_);
    ~SliderLP();
    
    void resized() override;

    float getLowpassFeedbackGain();
    
    float getLowpassGain();
   
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SliderLP)
    
    void sliderValueChanged	(Slider * 	slider);
    
    LowpassFilter& lowpassFilter;
    Audio& audioRef;
    
    Slider gainSlider;
    Slider feedbackGainSlider;
    Label  gainLabel;
    Label feedbackGainLabel;
    
    float gain;
    float level;
    
};


#endif /* defined(__JuceBasicWindow__SliderLP__) */
