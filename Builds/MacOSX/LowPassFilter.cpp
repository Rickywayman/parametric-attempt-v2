//
//  LowPassFilter.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 12/01/2016.
//
//

#include "LowPassFilter.h"


LowpassFilter::LowpassFilter()
{
    setDelayInSamples1(1.f);
    
    
}

LowpassFilter::~LowpassFilter()
{
    
}

//==============================================================================

float LowpassFilter::filter1(float input)
{
  
    float delayLineOutput1 = delayLineRead1();
    
    float output = (feedbackGain1 * input) + ((1.f-feedbackGain1) * delayLineOutput1);
    
    delayLineWrite1(output);
    
    return output;
}
