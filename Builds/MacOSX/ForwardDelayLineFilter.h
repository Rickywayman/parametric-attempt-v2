//
//  ForwardDelayLineFilter.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 13/01/2016.
//
//

#ifndef __JuceBasicWindow__ForwardDelayLineFilter__
#define __JuceBasicWindow__ForwardDelayLineFilter__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"


#define ASSUMEDSAMPLERATE 44100

class ForwardDelayLineFilter
{
public:
    //==============================================================================
    /**
     Constructor
     */
    ForwardDelayLineFilter();
    
    /**
     Destructor
     */
    virtual ~ForwardDelayLineFilter();
    //==============================================================================
    /**
     Set the delay time in samples of the delayline - can be fractional
     */
    void setForwardDelayInSamples1(float delayInSamples);
    
    /**
     Sets the feedback gain
     */
    void setforwardFeedbackGain1(float val);
    
    /**
     Function to perform the filter processing should call delayLineRead and delayLineWrite at some point
     in the process
     */
    virtual float feedforwardFilter1(float feedforwardInput1)=0;
    
protected:
    /**
     Reads from the delayline using linear interpolation - should be called once per filter call and prior
     to delayLineWrite or the delay will be one sample inacurate
     */
    float forwarddelayLineRead1();
    
    /**
     Writes to the delayline - should be called once per filter call and after to delayLineRead or
     the delay will be one sample inacurate
     */
    void forwarddelayLineWrite1(float input);
    float forwardfeedbackGain1;
    
private:
    float forwarddelayLine1[ASSUMEDSAMPLERATE];
    float forwardreadDelaySamples1;
    int forwardwriteIndex1;
    
    
    
};



#endif /* defined(__JuceBasicWindow__ForwardDelayLineFilter__) */
