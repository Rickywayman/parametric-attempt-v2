//
//  SliderHP.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 14/01/2016.
//
//

#include "SliderHP.h"

SliderHP::SliderHP (HighpassFilter& highpassFilter_, Audio& audio_) : highpassFilter (highpassFilter_), audioRef(audio_)
{
    // Gain Slider
    addAndMakeVisible(&gainHighSlider);
    gainHighSlider.setRange(0.0, 1.0, 0.01);
    gainHighSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    gainHighSlider.addListener(this);
    gainHighSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    gainHighSlider.setValue(1.0);
    //gainSlider.setValue(0.8, true);
    
    // Feedback Slider
    addAndMakeVisible(&feedbackHighGainSlider);
    feedbackHighGainSlider.setRange(0.0, 1.0, 0.01);
    feedbackHighGainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    feedbackHighGainSlider.addListener(this);
    feedbackHighGainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    feedbackHighGainSlider.setValue(1.0);
    //feedbackGainSlider.setValue(0.5, true);
    
    // Gain Label
    gainHighLabel.setText("gain", dontSendNotification);
    gainHighLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&gainHighLabel);
    
    // Feedback Label
    feedbackHighGainLabel.setText("feedback", dontSendNotification);
    feedbackHighGainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&feedbackHighGainLabel);}

SliderHP::~SliderHP()
{
    
}

void SliderHP::resized()
{
    int sliderHighSize = (getWidth()-20)/3;
    if(sliderHighSize > (getHeight()-60)) sliderHighSize = getHeight() - 60;
    
    gainHighLabel.setBounds(10, 0, sliderHighSize, 20);
    feedbackHighGainLabel.setBounds(10+sliderHighSize+sliderHighSize, 0, sliderHighSize, 20);
    
    gainHighSlider.setBounds(10, 20, sliderHighSize, sliderHighSize);
    feedbackHighGainSlider.setBounds(10+sliderHighSize+sliderHighSize, 20, sliderHighSize, sliderHighSize);
}

/** */
void SliderHP::sliderValueChanged (Slider* slider)
{
    if (slider == &gainHighSlider)
    {
        gain = slider->getValue();
        gain = gain*gain*gain; //cubic rule
        audioRef.setHighpassGain(gain);
        //DBG("highpass gain" << gain);
    }
    else if (slider == &feedbackHighGainSlider)
    {
        float feedbackGain = slider->getValue();
        feedbackGain = feedbackGain*feedbackGain*feedbackGain;
        highpassFilter.setFeedbackGain1(feedbackGain);
        //DBG ("slider output   " << feedbackGain);
    }
}

/** Function to pass lowpassGain variable to MainComponent*/
float SliderHP::getHighpassGain()
{
    return gain;
}

// Class End

