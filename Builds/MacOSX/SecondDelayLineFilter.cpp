//
//  SecondDelayLineFilter.cpp
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 25/01/2016.
//
//

#include "SecondDelayLineFilter.h"


SecondDelayLineFilter::SecondDelayLineFilter()
{
    for (int counter = 0; counter < ASSUMEDSAMPLERATE; counter++)
        delayLine2[counter] = 0.f;
    readDelaySamples2 = 1.f;
    writeIndex2 = 0;
    feedbackGain2 = 0.f;
}
SecondDelayLineFilter::~SecondDelayLineFilter()
{
    
}

void SecondDelayLineFilter::setDelayInSamples2(float delayInSamples)
{
    if(delayInSamples >= 0.f && delayInSamples <= ASSUMEDSAMPLERATE-1)
        readDelaySamples2 = delayInSamples;
}

void SecondDelayLineFilter::setFeedbackGain2(float val)
{
    
    DBG ("Second Delay Input   " << val);
    if (val >= 0.f && val <= 1.0)
    {
        feedbackGain2 = val;
    }
}

float SecondDelayLineFilter::delayLineRead2()
{
    float readIndex = writeIndex2 - readDelaySamples2;
    if (readIndex < 0)
        readIndex += ASSUMEDSAMPLERATE;
    
    int pos1 = static_cast<int>(readIndex);
    int pos2 = pos1 + 1;
    if(pos2 == ASSUMEDSAMPLERATE)
        pos2 = 0;
    
    float fraction = readIndex - static_cast<int>(pos1);
    float amprange = delayLine2[pos2] - delayLine2[pos1];
    float output = delayLine2[pos1] + (fraction * amprange);
    
    return output;
}

void SecondDelayLineFilter::delayLineWrite2(float input)
{
    if(++writeIndex2 == ASSUMEDSAMPLERATE)
        writeIndex2 = 0;
    delayLine2[writeIndex2] = input;
}
