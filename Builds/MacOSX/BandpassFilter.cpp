//
//  BandpassFilter.cpp
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 25/01/2016.
//
//

#include "BandpassFilter.h"

/** */
BandpassFilter::BandpassFilter()
{
    
    setDelayInSamples1(1.f);
    setDelayInSamples2(2.f);
    
    setForwardDelayInSamples1(1.f);
    setForwardDelayInSamples2(2.f);
    
    
}

/** */
BandpassFilter::~BandpassFilter()
{
    
}

//==============================================================================

/** */
float BandpassFilter::filter1(float input)
{
    
    float delayLineOutput1 = delayLineRead1();
    
    float output = ((1.f-feedbackGain1) * delayLineOutput1);
    
    delayLineWrite1(output);
    
    return output;
}

/** */
float BandpassFilter::filter2(float input)
{
    
    
    float delayLineOutput2 = delayLineRead2();
    
    float output = ((1.f-feedbackGain2) * delayLineOutput2);
    
    delayLineWrite2(output);
    
    return output;
}

/** */
float BandpassFilter::feedforwardFilter1(float forwardinput1)
{
    
    //float delayLineOutput = delayLineRead();
    float feedforward1 = forwarddelayLineRead1();
    
    //float output = (feedbackGain * forwardinput) + ((1.f-feedbackGain) * delayLineOutput);
    
    float output = (1.f-forwardfeedbackGain1) * feedforward1;
    
    //delayLineWrite(output);
    forwarddelayLineWrite1(output);
    
    return output;
    
    
}

/** */
float BandpassFilter::feedforwardFilter2(float forwardinput2)
{
    
    //float delayLineOutput = delayLineRead();
    float feedforward2 = forwarddelayLineRead2();
    
    //float output = (feedbackGain * forwardinput) + ((1.f-feedbackGain) * delayLineOutput);
    
    float output = (1.f-forwardfeedbackGain2) * feedforward2;
    
    //delayLineWrite(output);
    forwarddelayLineWrite2(output);
    
    return output;
    
    
}


