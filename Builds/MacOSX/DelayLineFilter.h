//
//  DelayLineFilter.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 12/01/2016.
//
//

#ifndef __JuceBasicWindow__DelayLineFilter__
#define __JuceBasicWindow__DelayLineFilter__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"


#define ASSUMEDSAMPLERATE 44100

class DelayLineFilter
{
public:
    //==============================================================================
    /**
     Constructor
     */
    DelayLineFilter();
    
    /**
     Destructor
     */
    virtual ~DelayLineFilter();
    //==============================================================================
    /**
     Set the delay time in samples of the delayline - can be fractional
     */
    void setDelayInSamples1 (float delayInSamples);
    
    /**
     Sets the feedback gain
     */
    void setFeedbackGain1 (float val);
    
    /**
     Function to perform the filter processing should call delayLineRead and delayLineWrite at some point
     in the process
     */
    virtual float filter1 (float input)=0;
    
    float readDelaySamples1;
    
protected:
    /**
     Reads from the delayline using linear interpolation - should be called once per filter call and prior
     to delayLineWrite or the delay will be one sample inacurate
     */
    float delayLineRead1();
    
    /**
     Writes to the delayline - should be called once per filter call and after to delayLineRead or
     the delay will be one sample inacurate
     */
    void delayLineWrite1(float input);
    float feedbackGain1;
    
private:
    float delayLine1[ASSUMEDSAMPLERATE];
    
    int writeIndex1;
    
    
    
};

#endif /* defined(__JuceBasicWindow__DelayLineFilter__) */
