//
//  HighpassFilter.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 12/01/2016.
//
//

#ifndef __JuceBasicWindow__HighpassFilter__
#define __JuceBasicWindow__HighpassFilter__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "DelayLineFilter.h"

/**
 Class that runs a simple one-pole lowpass filter
 */
class HighpassFilter : public DelayLineFilter
{
public:
    //==============================================================================
    /**
     Constructor
     */
    HighpassFilter();
    
    /**
     Destructor
     */
    ~HighpassFilter();
    //==============================================================================
    /**
     Function that executes a simple one-pole lowpass filter
     */
    float filter1(float input) override;
    
private:
    
};

#endif /* defined(__JuceBasicWindow__HighpassFilter__) */
