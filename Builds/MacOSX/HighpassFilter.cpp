//
//  HighpassFilter.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 12/01/2016.
//
//

#include "HighpassFilter.h"


HighpassFilter::HighpassFilter()
{
    setDelayInSamples1(1.f);
}

HighpassFilter::~HighpassFilter()
{
    
}

//==============================================================================

float HighpassFilter::filter1(float input)
{
    float delayLineOutput1 = delayLineRead1();
    
    float output = (feedbackGain1 * input) + ((1.f-feedbackGain1) * delayLineOutput1);
    
    delayLineWrite1(output);
    
    return output * -1;
}

